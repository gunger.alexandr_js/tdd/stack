const Stack = require('../stack');

let stack
beforeEach(function() {
  stack = new Stack(3);
});

afterEach(function() {
  stack.items = [];
 })

describe('stack', function() {
  it('Should constructs the stack with a given capacity', function() {
    expect(stack.items).toEqual([]);
    expect(stack.capacity).toBe(3);
  });

  it('Should have an isEmpty function that returns true if the stack is empty and false otherwise', function() {
    expect(stack.isEmpty()).toBe(true);
    stack.items.push(2);
    expect(stack.isEmpty()).toBe(false);
  });

  describe('stack.push', function() {
    it('Should add a new element on top of the stack', function() {
      stack.push(2);
      expect(stack.items[stack.items.length - 1]).toBe(2);
    });

    it('Should return the new element pushed at the top of the stack', function() {
      let elementPushed = stack.push(2)
      expect(elementPushed).toBe(2)
    })

    it('Should return full if one tries to push at the top of the stack while it is full', function() {
      stack.items = [1, 2, 3];
      let state = stack.push(4);
      expect(stack.items[stack.items.length - 1]).toBe(3);
      expect(state).toBe('Full');
    });
  });

  
  describe('stack.pop', function() {
    it('Should remove element from top of the stack', function() {
      stack.items = [1, 2, 3]
      stack.pop();
      expect(stack.items.length).toBe(2);
    });

    it('Should remove element from top of the stack and items have two elements [first, second]', function() {
      stack.items = [1, 2, 3]
      stack.pop();
      expect(stack.items).toEqual([1, 2]);
    });

    it('Should return Empty if items empty', function() {
      var state = stack.pop();
      expect(state).toBe("Empty");
    });
  });
  
  describe('stack.peek', function() {
    it('Should add head element to stack', function() {
      stack.items = [2, 3];
      stack.peak(4);
      expect(stack.items[stack.items.length - 1]).toBe(3);
      expect(stack.items).toEqual([4,2,3]);
    });
  });
});