function Stack(capacity){
    this.items = [];
    
    this.capacity = capacity;
    
    this.isEmpty = function(){
        return this.items.length === 0;
    };
    
    this.push = function(element){
        if (this.isFull()) {
            return 'Full';
        }

        this.items.push(element);
        return element;
    };
    
    this.isFull = function(){
        return this.items.length === capacity;
    };

    this.pop = function(){
        if (this.isEmpty()){
            return "Empty";
        }
        this.items.pop();
    };

    this.peak = function(element){
        this.items.unshift(element);
    };
}

module.exports = Stack